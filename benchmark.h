#ifndef _SCD_BENCHMARK_H_
#define _SCD_BENCHMARK_H_

#include <iostream>
#include <string>
#include <chrono>
#include <map>
#include <algorithm>
#include <cassert>

namespace deb{
    struct BenchmarkTimer{
        std::chrono::microseconds acc;
        decltype(std::chrono::high_resolution_clock::now()) timeA;
    };
    static std::map<std::string, BenchmarkTimer>bm_timers;

    inline void start_bm_timer(const std::string& name){
        auto& timer = bm_timers.insert({name, BenchmarkTimer{}}).first->second;
        assert(timer.timeA == decltype(timer.timeA){});
        timer.timeA = std::chrono::high_resolution_clock::now();
    }

    inline void stop_bm_timer(const std::string& name){
        auto timeB = std::chrono::high_resolution_clock::now();
        auto& timer = bm_timers.insert({name, BenchmarkTimer{}}).first->second;
        assert(timer.timeA != decltype(timer.timeA){});
        timer.acc += std::chrono::duration_cast<std::chrono::microseconds>(timeB-timer.timeA);
        timer.timeA = decltype(timer.timeA){};
    }

    inline std::ostream& operator<<(std::ostream& os, decltype(bm_timers) timers){
        int max_len=0;
        for(auto timer : timers)
            max_len = std::max(max_len, (int)timer.first.length());

        os << "====BENCHMARK TIMERS====:\n";
        for(auto timer : timers)
            os << std::string(' ', max_len-timer.first.length()) << timer.first << " -> " << timer.second.acc.count()/1000 << "ms\n";
        os << "========================" << std::endl;
        return os;
    }
}

#endif