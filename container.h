#ifndef _SCD_CONTAINER_H_
#define _SCD_CONTAINER_H_

#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <stack>

namespace deb{
    /// STD PAIR PRINTER
    template<typename FT, typename ST>
    std::ostream& operator<<(std::ostream& os, std::pair<FT, ST> p){
        return os << '(' << p.first << ", " << p.second << ')';
    }

    /// GENERIC CONTAINER PRINTER
    template<typename CT>
    struct ContainerPrinter{
        const CT& ref;
        const std::string delimiter, begin, end;
        inline friend std::ostream& operator<<(std::ostream& os, const ContainerPrinter<CT>& container){
            os << container.begin;
            bool first=true;
            for(auto &item : container.ref){
                if(first)
                    first = false;
                else
                    os << container.delimiter;
                os << item;
            }
            os << container.end;
            return os;
        }
    };

    /// GENERATOR FUNCTION - GENERIC AND SPECIALIZATIONS
    template<typename CT>
    inline ContainerPrinter<CT> Container(const CT& container, const std::string& delim=" "){
        return ContainerPrinter<CT>{.ref=container, .delimiter=delim, .begin="{", .end="}"};
    }

    // specialization for queue
    template<typename IT>
    inline ContainerPrinter<std::vector<IT>> Container(std::queue<IT> container, const std::string& delim=" "){
        static std::vector<IT>V;
        V.resize(container.size());
        for(int i=(int)V.size()-1;i>=0;i--){
            V[i] = container.front();
            container.pop();
        }
        return ContainerPrinter<std::vector<IT>>{.ref=V, .delimiter=delim, .begin="[=>", .end="=>]"};
    }

    // specialization for priority_queue
    template<typename IT>
    inline ContainerPrinter<std::vector<IT>> Container(std::priority_queue<IT> container, const std::string& delim=" "){
        static std::vector<IT>V;
        V.resize(container.size());
        for(int i=(int)V.size()-1;i>=0;i--){
            V[i] = container.top();
            container.pop();
        }
        return ContainerPrinter<std::vector<IT>>{.ref=V, .delimiter=delim, .begin="[", .end="<=>]"};
    }

    // specialization for stack
    template<typename IT>
    inline ContainerPrinter<std::vector<IT>> Container(std::stack<IT> container, const std::string& delim=" "){
        static std::vector<IT>V;
        V.resize(container.size());
        for(int i=(int)V.size()-1;i>=0;i--){
            V[i] = container.top();
            container.pop();
        }
        return ContainerPrinter<std::vector<IT>>{.ref=V, .delimiter=delim, .begin="[", .end="<=>]"};
    }
}

#endif