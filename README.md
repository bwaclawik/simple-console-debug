# Simple Console Debug library

Currently, the library supports:
- printing of most STL containers ( `deb::Container(...)` )
- benchmarking ( `deb::start_bm_timer(...)` , `deb::stop_bm_timer(...)` )
- auto tree-like indentation for recursion ( `deb::Indent` )