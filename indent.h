#ifndef _SCD_INDENT_H_
#define _SCD_INDENT_H_

#include <iostream>
#include <string>

namespace deb{
    namespace hidden{
        const std::string IndentConfigBeginMark="▼\n";
        const std::string IndentConfigEndMark="▲\n";
        const std::string IndentConfigBasicUnit="┃   ";
        const std::string IndentConfigBranch="┣━━╾";
    }

    namespace hidden{
        static int IndentLevel=0;
        static bool IndentInit=false;
        static std::ostream* IndentStream=nullptr;
    }
    
    struct Indent{
        Indent(){
            if(hidden::IndentLevel++ == 0)
                hidden::IndentInit=true;
        }
        ~Indent(){
            if(--hidden::IndentLevel == 0)
                *hidden::IndentStream << hidden::IndentConfigEndMark;
        }
        struct {} head;
        friend std::ostream& operator<<(std::ostream& os, const Indent &dummy){
            (void)dummy;
            if(hidden::IndentInit){
                os << hidden::IndentConfigBeginMark;
                hidden::IndentInit = false;
                hidden::IndentStream = &os;
            }
            for(int i=0;i<hidden::IndentLevel;i++)
                os << hidden::IndentConfigBasicUnit;
            return os;
        }
        friend std::ostream& operator<<(std::ostream& os, const decltype(head) &dummy){
            (void)dummy;
            if(hidden::IndentInit){
                os << hidden::IndentConfigBeginMark;
                hidden::IndentInit = false;
                hidden::IndentStream = &os;
            }
            for(int i=1;i<hidden::IndentLevel;i++)
                os << hidden::IndentConfigBasicUnit;
            os << hidden::IndentConfigBranch;
            return os;
        }
    };
}

#endif